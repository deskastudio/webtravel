<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /* ! tailwindcss v3.2.4 | MIT License | https://tailwindcss.com */*,::after,::before{box-sizing:border-box;border-width:0;border-style:solid;border-color:#e5e7eb}::after,::before{--tw-content:''}html{line-height:1.5;-webkit-text-size-adjust:100%;-moz-tab-size:4;tab-size:4;font-family:Figtree, sans-serif;font-feature-settings:normal}body{margin:0;line-height:inherit}hr{height:0;color:inherit;border-top-width:1px}abbr:where([title]){-webkit-text-decoration:underline dotted;text-decoration:underline dotted}h1,h2,h3,h4,h5,h6{font-size:inherit;font-weight:inherit}a{color:inherit;text-decoration:inherit}b,strong{font-weight:bolder}code,kbd,pre,samp{font-family:ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}table{text-indent:0;border-color:inherit;border-collapse:collapse}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;font-weight:inherit;line-height:inherit;color:inherit;margin:0;padding:0}button,select{text-transform:none}[type=button],[type=reset],[type=submit],button{-webkit-appearance:button;background-color:transparent;background-image:none}:-moz-focusring{outline:auto}:-moz-ui-invalid{box-shadow:none}progress{vertical-align:baseline}::-webkit-inner-spin-button,::-webkit-outer-spin-button{height:auto}[type=search]{-moz-animation-timing-function:textfield;outline-offset:-2px}::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}summary{display:list-item}blockquote,dd,dl,figure,h1,h2,h3,h4,h5,h6,hr,p,pre{margin:0}fieldset{margin:0;padding:0}legend{padding:0}menu,ol,ul{list-style:none;margin:0;padding:0}textarea{resize:vertical}input::placeholder,textarea::placeholder{opacity:1;color:#9ca3af}[role=button],button{cursor:pointer}:disabled{cursor:default}audio,canvas,embed,iframe,img,object,svg,video{display:block;vertical-align:middle}img,video{max-width:100%;height:auto}[hidden]{display:none}*, ::before, ::after{--tw-border-spacing-x:0;--tw-border-spacing-y:0;--tw-translate-x:0;--tw-translate-y:0;--tw-rotate:0;--tw-skew-x:0;--tw-skew-y:0;--tw-scale-x:1;--tw-scale-y:1;--tw-pan-x: ;--tw-pan-y: ;--tw-pinch-zoom: ;--tw-scroll-snap-strictness:proximity;--tw-ordinal: ;--tw-slashed-zero: ;--tw-numeric-figure: ;--tw-numeric-spacing: ;--tw-numeric-fraction: ;--tw-ring-inset: ;--tw-ring-offset-width:0px;--tw-ring-offset-color:#fff;--tw-ring-color:rgb(59 130 246 / 0.5);--tw-ring-offset-shadow:0 0 #0000;--tw-ring-shadow:0 0 #0000;--tw-shadow:0 0 #0000;--tw-shadow-colored:0 0 #0000;--tw-blur: ;--tw-brightness: ;--tw-contrast: ;--tw-grayscale: ;--tw-hue-rotate: ;--tw-invert: ;--tw-saturate: ;--tw-sepia: ;--tw-drop-shadow: ;--tw-backdrop-blur: ;--tw-backdrop-brightness: ;--tw-backdrop-contrast: ;--tw-backdrop-grayscale: ;--tw-backdrop-hue-rotate: ;--tw-backdrop-invert: ;--tw-backdrop-opacity: ;--tw-backdrop-saturate: ;--tw-backdrop-sepia: }::-webkit-backdrop{--tw-border-spacing-x:0;--tw-border-spacing-y:0;--tw-translate-x:0;--tw-translate-y:0;--tw-rotate:0;--tw-skew-x:0;--tw-skew-y:0;--tw-scale-x:1;--tw-scale-y:1;--tw-pan-x: ;--tw-pan-y: ;--tw-pinch-zoom: ;--tw-scroll-snap-strictness:proximity;--tw-ordinal: ;--tw-slashed-zero: ;--tw-numeric-figure: ;--tw-numeric-spacing: ;--tw-numeric-fraction: ;--tw-ring-inset: ;--tw-ring-offset-width:0px;--tw-ring-offset-color:#fff;--tw-ring-color:rgb(59 130 246 / 0.5);--tw-ring-offset-shadow:0 0 #0000;--tw-ring-shadow:0 0 #0000;--tw-shadow:0 0 #0000;--tw-shadow-colored:0 0 #0000;--tw-blur: ;--tw-brightness: ;--tw-contrast: ;--tw-grayscale: ;--tw-hue-rotate: ;--tw-invert: ;--tw-saturate: ;--tw-sepia: ;--tw-drop-shadow: ;--tw-backdrop-blur: ;--tw-backdrop-brightness: ;--tw-backdrop-contrast: ;--tw-backdrop-grayscale: ;--tw-backdrop-hue-rotate: ;--tw-backdrop-invert: ;--tw-backdrop-opacity: ;--tw-backdrop-saturate: ;--tw-backdrop-sepia: }::backdrop{--tw-border-spacing-x:0;--tw-border-spacing-y:0;--tw-translate-x:0;--tw-translate-y:0;--tw-rotate:0;--tw-skew-x:0;--tw-skew-y:0;--tw-scale-x:1;--tw-scale-y:1;--tw-pan-x: ;--tw-pan-y: ;--tw-pinch-zoom: ;--tw-scroll-snap-strictness:proximity;--tw-ordinal: ;--tw-slashed-zero: ;--tw-numeric-figure: ;--tw-numeric-spacing: ;--tw-numeric-fraction: ;--tw-ring-inset: ;--tw-ring-offset-width:0px;--tw-ring-offset-color:#fff;--tw-ring-color:rgb(59 130 246 / 0.5);--tw-ring-offset-shadow:0 0 #0000;--tw-ring-shadow:0 0 #0000;--tw-shadow:0 0 #0000;--tw-shadow-colored:0 0 #0000;--tw-blur: ;--tw-brightness: ;--tw-contrast: ;--tw-grayscale: ;--tw-hue-rotate: ;--tw-invert: ;--tw-saturate: ;--tw-sepia: ;--tw-drop-shadow: ;--tw-backdrop-blur: ;--tw-backdrop-brightness: ;--tw-backdrop-contrast: ;--tw-backdrop-grayscale: ;--tw-backdrop-hue-rotate: ;--tw-backdrop-invert: ;--tw-backdrop-opacity: ;--tw-backdrop-saturate: ;--tw-backdrop-sepia: }.relative{position:relative}.mx-auto{margin-left:auto;margin-right:auto}.mx-6{margin-left:1.5rem;margin-right:1.5rem}.ml-4{margin-left:1rem}.mt-16{margin-top:4rem}.mt-6{margin-top:1.5rem}.mt-4{margin-top:1rem}.-mt-px{margin-top:-1px}.mr-1{margin-right:0.25rem}.flex{display:flex}.inline-flex{display:inline-flex}.grid{display:grid}.h-16{height:4rem}.h-7{height:1.75rem}.h-6{height:1.5rem}.h-5{height:1.25rem}.min-h-screen{min-height:100vh}.w-auto{width:auto}.w-16{width:4rem}.w-7{width:1.75rem}.w-6{width:1.5rem}.w-5{width:1.25rem}.max-w-7xl{max-width:80rem}.shrink-0{flex-shrink:0}.scale-100{--tw-scale-x:1;--tw-scale-y:1;transform:translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y))}.grid-cols-1{grid-template-columns:repeat(1, minmax(0, 1fr))}.items-center{align-items:center}.justify-center{justify-content:center}.gap-6{gap:1.5rem}.gap-4{gap:1rem}.self-center{align-self:center}.rounded-lg{border-radius:0.5rem}.rounded-full{border-radius:9999px}.bg-gray-100{--tw-bg-opacity:1;background-color:rgb(243 244 246 / var(--tw-bg-opacity))}.bg-white{--tw-bg-opacity:1;background-color:rgb(255 255 255 / var(--tw-bg-opacity))}.bg-red-50{--tw-bg-opacity:1;background-color:rgb(254 242 242 / var(--tw-bg-opacity))}.bg-dots-darker{background-image:url("data:image/svg+xml,%3Csvg width='30' height='30' viewBox='0 0 30 30' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1.22676 0C1.91374 0 2.45351 0.539773 2.45351 1.22676C2.45351 1.91374 1.91374 2.45351 1.22676 2.45351C0.539773 2.45351 0 1.91374 0 1.22676C0 0.539773 0.539773 0 1.22676 0Z' fill='rgba(0,0,0,0.07)'/%3E%3C/svg%3E")}.from-gray-700\/50{--tw-gradient-from:rgb(55 65 81 / 0.5);--tw-gradient-to:rgb(55 65 81 / 0);--tw-gradient-stops:var(--tw-gradient-from), var(--tw-gradient-to)}.via-transparent{--tw-gradient-to:rgb(0 0 0 / 0);--tw-gradient-stops:var(--tw-gradient-from), transparent, var(--tw-gradient-to)}.bg-center{background-position:center}.stroke-red-500{stroke:#ef4444}.stroke-gray-400{stroke:#9ca3af}.p-6{padding:1.5rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.text-center{text-align:center}.text-right{text-align:right}.text-xl{font-size:1.25rem;line-height:1.75rem}.text-sm{font-size:0.875rem;line-height:1.25rem}.font-semibold{font-weight:600}.leading-relaxed{line-height:1.625}.text-gray-600{--tw-text-opacity:1;color:rgb(75 85 99 / var(--tw-text-opacity))}.text-gray-900{--tw-text-opacity:1;color:rgb(17 24 39 / var(--tw-text-opacity))}.text-gray-500{--tw-text-opacity:1;color:rgb(107 114 128 / var(--tw-text-opacity))}.underline{-webkit-text-decoration-line:underline;text-decoration-line:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.shadow-2xl{--tw-shadow:0 25px 50px -12px rgb(0 0 0 / 0.25);--tw-shadow-colored:0 25px 50px -12px var(--tw-shadow-color);box-shadow:var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow)}.shadow-gray-500\/20{--tw-shadow-color:rgb(107 114 128 / 0.2);--tw-shadow:var(--tw-shadow-colored)}.transition-all{transition-property:all;transition-timing-function:cubic-bezier(0.4, 0, 0.2, 1);transition-duration:150ms}.selection\:bg-red-500 *::selection{--tw-bg-opacity:1;background-color:rgb(239 68 68 / var(--tw-bg-opacity))}.selection\:text-white *::selection{--tw-text-opacity:1;color:rgb(255 255 255 / var(--tw-text-opacity))}.selection\:bg-red-500::selection{--tw-bg-opacity:1;background-color:rgb(239 68 68 / var(--tw-bg-opacity))}.selection\:text-white::selection{--tw-text-opacity:1;color:rgb(255 255 255 / var(--tw-text-opacity))}.hover\:text-gray-900:hover{--tw-text-opacity:1;color:rgb(17 24 39 / var(--tw-text-opacity))}.hover\:text-gray-700:hover{--tw-text-opacity:1;color:rgb(55 65 81 / var(--tw-text-opacity))}.focus\:rounded-sm:focus{border-radius:0.125rem}.focus\:outline:focus{outline-style:solid}.focus\:outline-2:focus{outline-width:2px}.focus\:outline-red-500:focus{outline-color:#ef4444}.group:hover .group-hover\:stroke-gray-600{stroke:#4b5563}.z-10{z-index: 10}@media (prefers-reduced-motion: no-preference){.motion-safe\:hover\:scale-\[1\.01\]:hover{--tw-scale-x:1.01;--tw-scale-y:1.01;transform:translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y))}}@media (prefers-color-scheme: dark){.dark\:bg-gray-900{--tw-bg-opacity:1;background-color:rgb(17 24 39 / var(--tw-bg-opacity))}.dark\:bg-gray-800\/50{background-color:rgb(31 41 55 / 0.5)}.dark\:bg-red-800\/20{background-color:rgb(153 27 27 / 0.2)}.dark\:bg-dots-lighter{background-image:url("data:image/svg+xml,%3Csvg width='30' height='30' viewBox='0 0 30 30' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1.22676 0C1.91374 0 2.45351 0.539773 2.45351 1.22676C2.45351 1.91374 1.91374 2.45351 1.22676 2.45351C0.539773 2.45351 0 1.91374 0 1.22676C0 0.539773 0.539773 0 1.22676 0Z' fill='rgba(255,255,255,0.07)'/%3E%3C/svg%3E")}.dark\:bg-gradient-to-bl{background-image:linear-gradient(to bottom left, var(--tw-gradient-stops))}.dark\:stroke-gray-600{stroke:#4b5563}.dark\:text-gray-400{--tw-text-opacity:1;color:rgb(156 163 175 / var(--tw-text-opacity))}.dark\:text-white{--tw-text-opacity:1;color:rgb(255 255 255 / var(--tw-text-opacity))}.dark\:shadow-none{--tw-shadow:0 0 #0000;--tw-shadow-colored:0 0 #0000;box-shadow:var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow)}.dark\:ring-1{--tw-ring-offset-shadow:var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);--tw-ring-shadow:var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);box-shadow:var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000)}.dark\:ring-inset{--tw-ring-inset:inset}.dark\:ring-white\/5{--tw-ring-color:rgb(255 255 255 / 0.05)}.dark\:hover\:text-white:hover{--tw-text-opacity:1;color:rgb(255 255 255 / var(--tw-text-opacity))}.group:hover .dark\:group-hover\:stroke-gray-400{stroke:#9ca3af}}@media (min-width: 640px){.sm\:fixed{position:fixed}.sm\:top-0{top:0px}.sm\:right-0{right:0px}.sm\:ml-0{margin-left:0px}.sm\:flex{display:flex}.sm\:items-center{align-items:center}.sm\:justify-center{justify-content:center}.sm\:justify-between{justify-content:space-between}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width: 768px){.md\:grid-cols-2{grid-template-columns:repeat(2, minmax(0, 1fr))}}@media (min-width: 1024px){.lg\:gap-8{gap:2rem}.lg\:p-8{padding:2rem}}
        </style>

        <link rel="stylesheet" href="style.css">

        <!-- Scripts -->
        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    </head>
<body class="body">
    <!-- navbar start -->
    <nav class="navbar navbar-expand-lg navbar-light fs-5 shadow p-3 bg-body z-10">
        <div class="container">
            <img src="{{asset('storage/images/image 1.svg')}}" alt="..." class="shadow rounded-pill">
            <a class="navbar-brand ms-2" href="#">ViztaGo</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link nav-link-hover active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-hover" href="#footer" tabindex="-1" aria-disabled="true">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-hover" href="#caroseul" tabindex="-1" aria-disabled="true">Go Explore</a>
                    </li>
                </ul>
                <a href="/signup" type="button" class="btn1 btn-primary btn-sm me-2 shadow" style="text-decoration: none; color: #112F65;">Sign Up</a>
                <a href="login" type="button" class="btn2 btn-secondary btn-sm shadow" style="color:white; text-decoration: none;">Login</a>
            </div>
        </div>
    </nav>
    <!-- navbar end -->

    <!-- hero section start-->
    <section id="hero">
            <div class="container">
                <div class="d-flex align-items-center flex-md-row flex-column-reverse">
                    <div class="col-12 col-md-8">
                        <div class="item">
                            <p class="fw-bold col-lg-12 col-md-6" style="font-size: 50px;">Jelajahi Keindahan Indonesia hanya dengan satu sentuhan</p>
                            <h1 class="col-lg-10 col-md-6" style="font-size: 23px; line-height: 35px; color:#141067;">Jelajahi Indonesia yang indah, dan nikmati rekomendasi wisata dari kami dimana saja dan kapan saja</h1>
                            <a href="#caroseul" class="btn btn-sm fs-4 mt-5 p-3 rounded-4" style="background-color: #141067;"><span class="p-4 shadow-lg" style="color: #EAF4F9;">Jelajahi Sekarang</span></a>
                        </div> 
                    </div>
                    <div class="col-12 col-md-4 d-flex justify-content-md-end justify-content-center">
                        <img src="{{asset('storage/images/image 2.png')}}" alt="..." class="me-5 mt-5 z-2">
                    </div>
                </div>
            </div>
            <img src="{{asset('storage/images/titik.png')}}" alt="..." class="position-absolute" style="top: -90px; left: -140px; z-index: 2">
            <img src="{{asset('storage/images/Rectangle.png')}}" alt="..." class="position-absolute" style="top: -160px; right: 0px; z-index: 1;">
    </section>
    <!-- hero section end -->

    <!-- caroseul start -->
    <section id="caroseul">
            <div class="container">
                <p class="destination p-3 fw-bold" style="color: white; text-shadow: 1px 1px 2px #EAF4F9;">Destination Highlight</p>
                    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row justify-content-center p-3">
                                    <div class="card mx-2" style="max-width: 500px;">
                                        <div class="row g-0">
                                            <div class="col-md-4 p-1">
                                            <img src="{{asset('storage/images/gunung1.png')}}" class="img-fluid rounded-start" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title fw-bold fs-4">Gunung Bromo</h5>
                                                <p class="card-text">Gunung bromo adalah ikon dari Jawa Timur yang sudah terkenal hingga ke mancanegara. Setiap tahun, ribuan turis asing kawasan Eropa berdatangan ke tempat ini, terutama pada musim panas.</p>
                                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card mx-2" style="max-width: 500px;">
                                        <div class="row g-0">
                                            <div class="col-md-4 p-1">
                                            <img src="{{asset('storage/images/gunung1.png')}}" class="img-fluid rounded-start" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title fw-bold fs-4">Kawah Ijen</h5>
                                                <p class="card-text">Gunung bromo adalah ikon dari Jawa Timur yang sudah  terkenal hingga ke mancanegara. Setiap tahun, ribuan turis asing kawasan Eropa berdatangan ke tempat ini, terutama pada musim panas. </p>
                                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row justify-content-center p-3">
                                    <div class="card mx-2" style="max-width: 500px;">
                                        <div class="row g-0">
                                            <div class="col-md-4 p-1">
                                            <img src="{{asset('storage/images/gunung1.png')}}" class="img-fluid rounded-start" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title fw-bold fs-4">Museum Angkut</h5>
                                                <p class="card-text">Gunung bromo adalah ikon dari Jawa Timur yang sudah  terkenal hingga ke mancanegara. Setiap tahun, ribuan turis asing kawasan Eropa berdatangan ke tempat ini, terutama pada musim panas. </p>
                                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card mx-2" style="max-width: 500px;">
                                        <div class="row g-0">
                                            <div class="col-md-4 p-1">
                                            <img src="{{asset('storage/images/gunung1.png')}}" class="img-fluid rounded-start" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title fw-bold fs-4">Labuan Bajo</h5>
                                                <p class="card-text">Gunung bromo adalah ikon dari Jawa Timur yang sudah  terkenal hingga ke mancanegara. Setiap tahun, ribuan turis asing kawasan Eropa berdatangan ke tempat ini, terutama pada musim panas. </p>
                                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                            </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row justify-content-center p-3">
                                    <div class="card mx-2" style="max-width: 500px;">
                                        <div class="row g-0">
                                            <div class="col-md-4 p-1">
                                            <img src="{{asset('storage/images/gunung1.png')}}" class="img-fluid rounded-start" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title fw-bold fs-4">Pantai Sawarna</h5>
                                                <p class="card-text">Gunung bromo adalah ikon dari Jawa Timur yang sudah  terkenal hingga ke mancanegara. Setiap tahun, ribuan turis asing kawasan Eropa berdatangan ke tempat ini, terutama pada musim panas. </p>
                                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card mx-2" style="max-width: 500px;">
                                        <div class="row g-0">
                                            <div class="col-md-4 p-1">
                                            <img src="{{asset('storage/images/gunung1.png')}}" class="img-fluid rounded-start" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title fw-bold fs-4">Candi Borobudur</h5>
                                                <p class="card-text">Gunung bromo adalah ikon dari Jawa Timur yang sudah  terkenal hingga ke mancanegara. Setiap tahun, ribuan turis asing kawasan Eropa berdatangan ke tempat ini, terutama pada musim panas. </p>
                                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                            </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
    
                    <!-- Travel Inspiration Start-->
                <p class="destination p-3 fw-bold" style="color: white; text-align: center; text-shadow: 1px 1px 2px #EAF4F9;">Travel Inspiration</p>
                <div class="row justify-content-between">
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Jatim Park</h2>
                            <a href="detail" class="btn btn-primary mt-5" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Pantai Klanyar</h2>
                            <a href="detail" class="btn btn-primary mt-5" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Gili Labak</h2>
                            <a href="detail" class="btn btn-primary mt-5" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Taman Nasional Baluran</h2>
                            <a href="detail" class="btn btn-primary mt-3" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between mt-4">
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Gunung Bromo</h2>
                            <a href="detail" class="btn btn-primary mt-5" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Museum Angkut</h2>
                            <a href="detail" class="btn btn-primary mt-5" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Gua Gong</h2>
                            <a href="detail" class="btn btn-primary mt-5" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                    <div class="card p-3 shadow" style="width: 20rem; border: none;">
                        <img src="{{asset('storage/images/jalan.png')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>⭐⭐⭐⭐</h2>
                            <h2 class="card-text fw-bold">Candi Penataran</h2>
                            <a href="detail" class="btn btn-primary mt-5" style="background-color: #141067; border: none;">Lihat Detail</a>
                        </div>
                    </div>
                </div>
                <!-- Travel Inspiration End-->
            </div>
    </section>
    <!-- caroseul end -->

    <!-- subscribe start-->
    <section id="subscribe">
            <div class="container" style="margin-top: 500px">
                <h1 class="text-light fw-bold p-4" style="text-align: center;">Subscribe for more Information</h1>
                <form class="row g-5 justify-content-center p-4 align-items-center">
                    <div class="col-auto">
                      <label for="inputEmail" class="visually-hidden">Email</label>
                      <input type="email" class="form-control rounded-5 fs-4" style="width: 450px; height: 70px;" id="inputEmail" placeholder="Enter your Email Address">
                    </div>
                    <div class="col-auto">
                      <button type="submit" class="btn rounded-5 fs-4" style="background-color: #141067; color:white; width: 200px; height: 50px;">Subscribe</button>
                    </div>
                </form>
            </div>
    </section>
    <!-- subscribe end-->


    {{-- Footer Start --}}
    <section id="footer">
        <nav class="navbar navbar-expand-lg fs-5 p-3 z-10">
            <div class="container">
                <img src="{{asset('storage/images/image 1.svg')}}" alt="..." class="shadow rounded-pill">
                <a class="navbar-brand ms-2" style="color: white;" href="#">ViztaGo</a>
                <div class="collapse navbar-collapse me-0" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 me-0">
                        <li class="nav-item">
                            <a class="nav-link" style="color: white;" aria-current="page" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color: white;" href="/login" tabindex="-1" aria-disabled="true">Login</a>
                        </li>
                    </ul>
                    <div class="d-flex justify-content-center align-items-center" style="gap: 40px;">
                        <a href="https://www.facebook.com/profile.php?id=100009457752082"><img src="{{asset('storage/images/facebook.svg')}}" alt="facebook"></a>
                        <a href="https://www.instagram.com/dmulyana_/"><img src="{{asset('storage/images/instagram.svg')}}" alt="instagram"></a>
                        <a href="https://twitter.com/dmulyana_"><img src="{{asset('storage/images/twitter.svg')}}" alt="twitter"></a>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container">
            <h5 class="p-5" style="text-align: center; color: white;">Copyright by Kelompok 5 All Right Reserved.</h5>
        </div>
    </section>
    {{-- Footer End --}}
</body>
</html>
